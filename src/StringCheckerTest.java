import java.util.Arrays;
import java.util.Collection;
 
import org.junit.Test;
//import org.junit.Before;

import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.junit.runner.RunWith;
import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class StringCheckerTest {
   private String input;
   private Boolean expectedResult;
	
   public StringCheckerTest(String input, Boolean expectedResult) {
      this.input = input;
      this.expectedResult = expectedResult;
   }

   @Parameterized.Parameters
   public static Collection string1() {
      return Arrays.asList(new Object[][] {
         { "chrome", true },
         { "windows", false },
         { "ie", true },
         { "browser", false },
         { "firefox", true }
      });
   }
   @Test
   public void testString() {
 if(expectedResult)
  {

	  assertEquals(true, expectedResult);
	  System.out.println("String is : " + input);
  }   
 }
}


