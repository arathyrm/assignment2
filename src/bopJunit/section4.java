
package bopJunit;
 

import static org.junit.Assert.assertEquals;

 

import org.junit.After;
import org.junit.Before;

 

import org.junit.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.RepeatedTest;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class section4 {
    WebDriver dr;
    
    @Before
    public void startApp() {
        System.setProperty("webdriver.chrome.driver", "D:\\Drivers\\chromedriver.exe");
        dr = new ChromeDriver();
        dr.get("http://shop.demoqa.com/");
    }
    
    @BeforeEach 
    public void beforeRepeat() {
        System.setProperty("webdriver.chrome.driver","D:\\Drivers\\chromedriver.exe");
        dr = new ChromeDriver();
        dr.get("http://shop.demoqa.com/");
    }
    
    //@Test(timeout = 1000)
    @RepeatedTest(2)
    public void test1() {
        assertEquals("http://shop.demoqa.com/",dr.getCurrentUrl());
    }
    
    @Disabled("test case 2 is disabled")
    @Test (timeout = 1000)
    public void test2() {
         assertEquals("Shoptools",dr.getTitle());
    }
    
    @AfterEach
    public void afterRepeat() {
        dr.close();
    }
    @After
    public void closeApp() {
        dr.close();
    }
}