package bopJunit;
import org.junit.*;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class section1{

	 WebDriver dr;

	 @After
		public void Close()
		{
			  dr.close();
		}
	    @Before
	    public void StartApp()
	    {
	    	System.setProperty("webdriver.chrome.driver", "D:\\Drivers\\chromedriver.exe");
	    	   dr=new ChromeDriver();
	    	    dr.get(" http://shop.demoqa.com/");

	    	   
	    }

		@Test
	    public void TC1()
	    {
	    	List<WebElement> links = dr.findElements(By.tagName("a"));
	    	boolean found = false;
	    	for(WebElement we : links) {
	    		if(we.getAttribute("href").equals("http://shop.demoqa.com/"))
	    		{
	    			found=true;
	    			break;
	    		}
	    	}
	    	
	    	 Assert.assertTrue(found);
	    }
	    @Test
	    public void TC2()
	    {
	    	 boolean st=dr.getCurrentUrl().contains("http://shop.demoqa.com/");
	         Assert.assertTrue(st);	
	    }
}    
