package bopJunit;


	import static org.junit.Assert.*;

	import java.util.Arrays;
	import java.util.Collection;

	import org.junit.*;
	import org.junit.rules.TestName;
	import org.junit.runner.RunWith;
	import org.junit.runners.Parameterized;
	import org.openqa.selenium.WebDriver;
	import org.openqa.selenium.chrome.ChromeDriver;
	import org.openqa.selenium.edge.EdgeDriver;
	import org.openqa.selenium.firefox.FirefoxDriver;
	import org.openqa.selenium.ie.InternetExplorerDriver;

	@RunWith(Parameterized.class)
	public class section3 {
		WebDriver dr;
		
		@Parameterized.Parameters()
		public static Collection<Object[]> data(){
			Object[][] data = new Object[][] {
				{"ie"},{"chrome"},{"edge"},{"firefox"}
			};
			return Arrays.asList(data);
		}
		
		@Parameterized.Parameter(0)
		public String s;
		
		@Before
		public void Before() {
			switch(s) {
			case "ie":
				System.setProperty("webdriver.ie.driver", "D:\\Drivers\\IEDriverServer_x64_3.14.0\\IEDriverServer.exe");
				dr = new InternetExplorerDriver();
				break;
			
			
			case "chrome" :
				System.setProperty("webdriver.chrome.driver", "D:\\Drivers\\chromedriver.exe");
				dr = new ChromeDriver();
				break;
				
			case "edge":
				System.setProperty("webdriver.edge.driver","D:\\Drivers\\MicrosoftWebDriver.exe");
				dr = new EdgeDriver();
				break;
			
			case "firefox":
				System.setProperty("webdriver.gecko.driver", "D:\\Drivers\\geckodriver-v0.26.0-win64\\geckodriver.exe");
				dr = new FirefoxDriver();
			}
			dr.get("http://shop.demoqa.com/");
		}
		
		@Test
		public void test1() throws InterruptedException {
			Thread.sleep(5000);
			assertEquals("http://shop.demoqa.com/",dr.getCurrentUrl());
		}
		
		@Test
		public void test2() throws InterruptedException {
		
			assertNotEquals("Shoptools",dr.getTitle());
		}
		
		@After
		public void closeApp() throws InterruptedException {
			
			dr.close();
		}
	}


